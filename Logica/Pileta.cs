﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public abstract class Pileta
    {
        public int CodigoPileta { get; set; }
        public int Litros { get; set; }
        public double Precio { get; set; }
        public enum Color { Azul=0, Celeste = 1, Gris=2 }

        //Corrección: Esto puede ser un método virtual que devuelva 0 como valor por defecto y se sobreescriba solo cuando se necesita.
        public abstract int Descuento();

    }
}
