﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Lona:Pileta
    {
        public decimal Alto { get; set; }
        public decimal Ancho { get; set; }
        public decimal Profundidad { get; set; }
        public bool Filtro { get; set; }
        public bool CubrePileta { get; set; }

        public override int Descuento()
        {
            return 8;
        }
    }
}
