﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        public List<Usuario> Personas { get; set; }
        public List<Pileta> Piletas { get; set; }
        public List<Venta> Ventas { get; set; }



        public Principal()
        {
            Personas = new List<Usuario>();
            Piletas = new List<Pileta>();
            Ventas = new List<Venta>();
        }

        public Usuario ObtenerUsuario(int dni)
        {
            //Corrección: Si no existe el DNI este código rompe porque el First devuelve un error, para usarlo como querés, necesitas usar el FirstOrDefault
            return Personas.First(x => x.Documento == dni);
        }

        //Corrección: Esto debería ser un método base en usuario.
        public string DetalleUsuario(int dni)
        {
            Usuario usuarioEncontrado = ObtenerUsuario(dni);
            if (usuarioEncontrado!=null)
            {
                if (usuarioEncontrado is Cliente)
                {
                    Cliente cteEncontrado = usuarioEncontrado as Cliente;

                    return cteEncontrado.NombreApellido + " - Telefono: " + cteEncontrado.Telefono + " " + cteEncontrado.Localidad.Provincia + " ," + cteEncontrado.Localidad.Nombre + " ," + cteEncontrado.Direccion;
                }
                else
                {
                    Empleado empEncontrado = usuarioEncontrado as Empleado;            
                    //“[Apellido], [Nombre] - Telefono: [Telefono] - [Provincia], [Ciudad], [Dirección] - Turno [Turno] - Área [Area]”
                    return empEncontrado.NombreApellido + " - Telefono: " + empEncontrado.Telefono + " - " + empEncontrado.Localidad.Provincia + " ," + empEncontrado.Localidad.Nombre +
                        " ," + empEncontrado.Direccion + " - ";//ver

                }

            }
            return "No se encontro coincidencia";                       
        }

        //Corrección: esto tiene que estár en usuario y sobreescribirse en cada subclase.
        public string MensajeAniversario(int dni)
        {
            Usuario usuarioEncontrado = ObtenerUsuario(dni);
            if (usuarioEncontrado != null)
            {
                if (usuarioEncontrado is Cliente)
                {
                    Cliente cteEncontrado = usuarioEncontrado as Cliente;
                    return cteEncontrado.VerificarAniversario();
                }
                else
                {
                    Empleado empEncontrado = usuarioEncontrado as Empleado;
                    return empEncontrado.VerificarCumple();

                }              
            }
            return "No se encontro coincidencia";
        }

        public bool RegistarCompraPileta(int dni, int codPileta, ref string mensaje)
        {
            Pileta piletaAcolocar = Piletas.First(x=>x.CodigoPileta == codPileta);
            if (piletaAcolocar!=null)
            {
                Usuario usuarioEncontrado = ObtenerUsuario(dni);
                if (usuarioEncontrado != null)
                {
                    if (usuarioEncontrado is Empleado)
                    {
                        //Corrección: Ya vimos que no es necesario hacer el cambio de estructuras para ejecutar el método
                        if (piletaAcolocar is Material)
                        {
                            Material descPileta = piletaAcolocar as Material;
                            descPileta.Descuento();                           
                        }
                        else
                        if (piletaAcolocar is Lona)
                        {
                            Lona descPileta = piletaAcolocar as Lona;
                            descPileta.Descuento();
                        }
                        else
                        {
                            Inflable descPileta = piletaAcolocar as Inflable;
                            descPileta.Descuento();
                        }
                        mensaje = "El registro se realizó correctamente";
                        
                    }
                    else
                    {
                        Cliente nuevoVenta = usuarioEncontrado as Cliente;
                        nuevoVenta.Instalacion = DateTime.Today;
                        nuevoVenta.CodigoPileta = codPileta;
                        mensaje = "Se registró la nueva pileta del cliente";
                        
                    }

                }
                else
                {
                    Cliente nuevoCliente = new Cliente();
                    nuevoCliente.CodigoPileta = codPileta;
                    nuevoCliente.Documento = dni;
                    mensaje = "Se dio de alta la venta y el cliente, recuerde que debe completar sus datos";
                    

                }
                Venta nuevaVenta = new Venta();
                nuevaVenta.Persona = usuarioEncontrado;
                nuevaVenta.Pileta = piletaAcolocar;
                Ventas.Add(nuevaVenta);
                return true;
            }
            

            return false;
        }














    }

    

    
}
