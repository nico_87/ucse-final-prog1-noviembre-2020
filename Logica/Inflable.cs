﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Inflable : Pileta
    {
        public decimal Diametro { get; set; }
        public decimal Profundidad { get; set; }
        public bool CubrePileta { get; set; }


        public override int Descuento()
        {
            return 0;
        }
    }
}
