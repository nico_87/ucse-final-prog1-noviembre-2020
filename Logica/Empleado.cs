﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Empleado : Usuario
    {
        public DateTime FechaNacimiento { get; set; }
        public enum Turno { Mañana=0,Tarde=1 }
        public enum Area { Gerencia = 0 , VentasEInstalacion = 1 }

        internal string VerificarCumple()
        {
            if (FechaNacimiento.DayOfYear == DateTime.Today.DayOfYear & FechaNacimiento.Month == DateTime.Today.Month)
            {
                return "¡Feliz cumpleaños compañero!";
            }
            return "";
        }
    }
}
