﻿namespace Logica
{
    public class Localidad
    {
        public string Nombre { get; set; }
        public string Provincia { get; set; }
        public int CodPostal { get; set; }

    }
}