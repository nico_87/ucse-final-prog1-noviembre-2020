﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Cliente : Usuario
    {
        public int CodigoPileta { get; set; }
        public DateTime Instalacion { get; set; }

        internal string VerificarAniversario()
        {
            if (Instalacion.DayOfYear == DateTime.Today.DayOfYear & Instalacion.Month==DateTime.Today.Month)
            {
                if (DateTime.Today.Year - Instalacion.Year == 1)
                {
                    return "¡Felicitaciones por su primer aniversario de instalación!";
                }
            }
            return "";
        }
    }
}
