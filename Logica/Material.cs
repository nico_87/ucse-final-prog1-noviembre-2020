﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Material:Pileta
    {
        public decimal Largo { get; set; }
        public decimal Ancho { get; set; }
        public double Profundidad { get; set; }
        public bool Trampolin { get; set; }
        public int Escaleras { get; set; }

        public override int Descuento()
        {
            if (Profundidad > 1.5)
            {
                if (Trampolin==true)
                {
                    return 10;
                }
            }
            return 5;
        }


    }
}
